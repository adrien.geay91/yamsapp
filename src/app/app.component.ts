import { Component ,Inject ,Input } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';
import { PassDataService } from './pass-data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private passDataService:PassDataService) { }

  title = 'yamsApp';

  nameJoueur1 = "Joueur 1";
  nameJoueur2 = "Joueur 2";
  chance=false;
  yams=false;
  brelan=false
  carre=false;
  full=false;
  gSuite=false
  pSuite=false;
  resultat=false;
  un=false;
  deux=false;
  trois=false;
  quatre=false;
  cinq=false;
  six=false;
  bonus=false;
  resultChance=0;
  resultYams=0;
  resultBrelan=0;
  resultCarre=0;
  resultFull=0;
  resultGSuite=0;
  resultPSuite=0;
  resultUn=0;
  resultDeux=0;
  resultTrois=0;
  resultQuatre=0;
  resultCinq=0;
  resultSix=0;
  resultBonus=0;
  resultChance2=0;
  resultYams2=0;
  resultBrelan2=0;
  resultCarre2=0;
  resultFull2=0;
  resultGSuite2=0;
  resultPSuite2=0;
  resultUn2=0;
  resultDeux2=0;
  resultTrois2=0;
  resultQuatre2=0;
  resultCinq2=0;
  resultSix2=0;
  resultBonus2=0;
  choixFinal!:string;
  scoreG=0;
  score1=0;
  score2=0;

  getNameJ1(name:any) {

  }

  //création des dé
  de1 = new deJeu(1,false);
  de2 = new deJeu(1,false);
  de3 = new deJeu(1,false);
  de4 = new deJeu(1,false);
  de5 = new deJeu(1,false);

  //affichage des dé
  d1=this.de1.getDe();
  d2=this.de2.getDe();
  d3=this.de3.getDe();
  d4=this.de4.getDe();
  d5=this.de5.getDe();

  //variable qui permet de compter le nombre de fois un joueur lance un dé
  compteurTrs=0;
  //variable qui permet d'afficher / cacher les joueurs
  joueur1=true;
  joueur2=false;

  scoreJoueur1!:number;
  //on génére des dé aléatoirement
  getRandomInt(min :any ,max:any)
  {
    this.d1 = Math.floor(Math.random() * (max - min) + min);
    this.d2 = Math.floor(Math.random() * (max - min) + min);
    this.d3 = Math.floor(Math.random() * (max - min) + min);
    this.d4 = Math.floor(Math.random() * (max - min) + min);
    this.d5 = Math.floor(Math.random() * (max - min) + min);
  }
  //fonction qui va être utilisé par le bouton 'lancer'
  getrand()
  {
    this.getRandomInt(1,7);
    this.compteurTrs++;
    //condition qui va permettre d'afficher / cacher les dé reservé par les joueurs
    if(this.compteurTrs%3==0){
      this.joueur1=!this.joueur1;
      this.joueur2=!this.joueur2;
      this.de1.show=false;
      this.de2.show=false;
      this.de3.show=false;
      this.de4.show=false;
      this.de5.show=false;
      this.passDataService.calculScore();
    }
    this.passDataService.calculNbrTours(this.compteurTrs);
  }

  //on recupere la donnée des dé et on l'envoie vers le service
  getdata(de:any)
  {
    this.passDataService.recupData(de.toString());
    console.log(de);
  }

  valideChoix(choix:any){

    this.choixFinal=choix;
    console.log(this.choixFinal);

  }
  recupeRusultChoix(j:any){
    if (this.choixFinal=="chance"){
      if(j==1)
      {
        this.resultChance=0;
        this.resultChance=this.passDataService.calculScore();
      }
      else{
        this.resultChance2=this.passDataService.calculScore();
      }
      
    }
    else if (this.choixFinal=="bonus") {
      if(j==1)
      {
        this.resultBonus=this.passDataService.calculScore();
      }
      else{
        this.resultBonus2=this.passDataService.calculScore();
      }
      
    } 
    console.log("le choix final",this.choixFinal)
    if (this.choixFinal=="un") {
      if(j==1)
      {
        this.resultUn=this.passDataService.calculNombre(1);
      }
      else{
        this.resultUn2=this.passDataService.calculNombre(1);
      }
    } 
    else if (this.choixFinal=="deux") {
      if(j==1)
      {
        this.resultDeux=this.passDataService.calculNombre(2);
      }
      else{
        this.resultDeux2=this.passDataService.calculNombre(2);
      }
    } 
    else if (this.choixFinal=="trois") {
      if(j==1)
      {
        this.resultTrois=this.passDataService.calculNombre(3);
      }
      else{
        this.resultTrois2=this.passDataService.calculNombre(3);
      }
      
    } 
    else if (this.choixFinal=="quatre") {
      if(j==1)
      {
        this.resultQuatre=this.passDataService.calculNombre(4);
      }
      else{
        this.resultQuatre2=this.passDataService.calculNombre(4);
      }
      
    } 
    else if (this.choixFinal=="cinq") {
      if(j==1)
      {
        this.resultCinq=this.passDataService.calculNombre(5);
      }
      else{
        this.resultCinq2=this.passDataService.calculNombre(5);
      }
      
    } 
    else if (this.choixFinal=="six") {
      if(j==1)
      {
        this.resultSix=this.passDataService.calculNombre(6);
      }
      else{
        this.resultSix2=this.passDataService.calculNombre(6);
      }
      
    } 
    else if (this.choixFinal=="brelan") {
      if(j==1)
      {
        this.resultBrelan=this.passDataService.calculSpeciaux("brelan");
      }
      else{
        this.resultBrelan2=this.passDataService.calculSpeciaux("brelan");
      }
      
    }
    /*else if (this.choixFinal=="full") {
      this.resultFull=this.passDataService.calculScore();
    } */
    else if (this.choixFinal=="carre") {
      if(j==1)
      {
        this.resultCarre=this.passDataService.calculSpeciaux("carre");
      }
      else{
        this.resultCarre2=this.passDataService.calculSpeciaux("carre");
      }
      
    } 
    else if (this.choixFinal=="pSuite") {
      if(j==1)
      {
        this.resultPSuite=this.passDataService.calculSuite("pSuite");
      }
      else{
        this.resultPSuite2=this.passDataService.calculSuite("pSuite");
      }
      
    } 
    else if (this.choixFinal=="gSuite") {
      if(j==1)
      {
        this.resultGSuite=this.passDataService.calculSuite("gSuite");
      }
      else{
        this.resultGSuite2=this.passDataService.calculSuite("gSuite");
      }
    } 
    else if (this.choixFinal=="yams") {
      if(j==1)
      {
        this.resultYams=this.passDataService.calculSpeciaux("yams");
      }
      else{
        this.resultYams2=this.passDataService.calculSpeciaux("yams");
      }
      
    } 
    console.log(this.choixFinal);
    console.log(this.resultDeux);
    this.passDataService.listede
  }
  calculGagnant(){
    this.score1=this.resultBonus+this.resultBrelan+this.resultCarre+this.resultChance+this.resultCinq+this.resultDeux+this.resultFull+this.resultGSuite+this.resultPSuite+this.resultQuatre+this.resultSix+this.resultTrois+this.resultUn+this.resultYams;
    this.score2=this.resultBonus2+this.resultBrelan2+this.resultCarre2+this.resultChance2+this.resultCinq2+this.resultDeux2+this.resultFull2+this.resultGSuite2+this.resultPSuite2+this.resultQuatre2+this.resultSix2+this.resultTrois2+this.resultUn2+this.resultYams2;
    if(this.score1>this.score2){
    alert("le joueur 1 a gangé : "+this.score1);
  }
    else{
      alert("le joueur 2 a gangé : "+this.score2);
    }


  }
}


class deJeu
  {
    de:number;
    show:boolean = false;

    constructor(de:number, show:boolean)
    {
      this.de = de;
      this.show = false; //variable qui va permettre d'afficher / cacher les dé
    }
    getDe()
    {
      return this.de;
    }
    //methode qui permet de cacher les dé
    hidefunction()
    {
      return this.show = !this.show;
    }
  }
