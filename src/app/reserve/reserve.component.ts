import { Component, OnInit } from '@angular/core';
import { PassDataService } from '../pass-data.service';

@Component({
  selector: 'app-reserve',
  templateUrl: './reserve.component.html',
  styleUrls: ['./reserve.component.scss']
})
export class ReserveComponent implements OnInit {

  public reserves: number[] = [];
  chance=false;
  //declaration du service
  constructor(private passDataService:PassDataService) { }

  recupresultat()
  {
    this.chance = !this.chance;
    console.log(this.chance)
  }
  ngOnInit()
  {
    //on envoie la liste des dé du composant père vers le composant fils
    this.reserves = this.passDataService.getDe();
  }
  /*
  showNumber(de:any) 
  {
    this.passDataService.recupData(de);
  }*/

}
