import { Component, OnInit } from '@angular/core';
import { PassDataService } from '../pass-data.service';

@Component({
  selector: 'app-joueur2',
  templateUrl: './joueur2.component.html',
  styleUrls: ['./joueur2.component.scss']
})
export class Joueur2Component implements OnInit {
  nameJ2 = "Joueur 2"
  display = 'block'
  displayName = 'none'

  //declaration du service
  constructor(private passDataService:PassDataService) { }

  ngOnInit(): void {
  }

  sendName(input: any){
     this.nameJ2 = input;
     this.display = "none";
     this.displayName = "block"
  }

}
