import { Component, OnInit } from '@angular/core';
import { PassDataService } from '../pass-data.service';

@Component({
  selector: 'app-joueur1',
  templateUrl: './joueur1.component.html',
  styleUrls: ['./joueur1.component.scss']
})
export class Joueur1Component implements OnInit {
  nameJ1 = "Joueur 1"
  display = 'block'
  displayName = 'none'
  //declaration du service
  constructor(private passDataService:PassDataService) { }

  ngOnInit(){

  }

  sendName(input: any){
     this.nameJ1 = input;
     this.display = "none";
     this.displayName = "block"
     this.passDataService.getName1(this.nameJ1);

  }

}
